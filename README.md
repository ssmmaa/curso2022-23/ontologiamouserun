[![logo](https://www.gnu.org/graphics/gplv3-127x51.png)](https://choosealicense.com/licenses/gpl-3.0/)

# Ontología
## Juegos de Tablero

En este documento se presenta el análisis, diseño e implementación para la ontología que se utilizarán en el desarrollo de práctica relacionada con el juego [Mouse Run](https://mouse-run.appspot.com/). El diseño de la ontología estará pensado para resolver las necesidades de comunicación de los agentes implicados en las prácticas. Estos agentes estarán diseñados para responder a los eventos necesarios para:

- Localizar a los agentes especializados:

	- Agentes organizadores que serán los encargados de crear las diferentes partidas para un juego con los ratones correspondientes. Serán los encargados de crear los agentes que completan una partida para un laberinto con la lista de ratones que se le ha suministrado.

	- Agentes jugadores para uno o varios juegos. Los agentes corresponden a los ratones que se diseñan que jueguen correctamente, es decir, no tienen la posibilidad para *hacer trampa*.
	
- Realizar las tareas necesarias para que los agentes cumplan correctamente con sus objetivos:

	- Localizar a los agentes jugadores que estén dispuestos a jugar un juego.
	- Localizar un agente organizador para que pueda completar ese juego.
	- Crear los agentes de partida necesarios para cada una de las partidas que componen un juego.
	- Completar los turnos necesarios para una partida individual entre los agentes jugadores.
	- Comunicar el resultado de las diferentes partidas para así completar el resultado del juego.

Como el diseño de la ontología estará enfocado en resolver las necesidades de comunicación entre los agentes se incluirán los diagramas UML para los diferentes protocolos de comunicación entre **agentes-FIPA** que deben implementarse para que todos los agentes puedan participar independientemente del grupo que los desarrolle.

## 1 Análisis de la ontología

Nuestra ontología debe responder apropiadamente a las siguientes preguntas:

1. ¿Cómo diferenciar a los diferentes agentes especializados?

2. ¿Cómo proponer a los diferentes jugadores que participen en un juego?

3. ¿Cómo debe completarse un juego?

4. ¿Cómo obtener el resultado del juego propuesto?

5. ¿Cómo generar las partidas que componen un juego?

6. ¿Cómo iniciar la partida? ¿Cómo completar un turno de una partida? ¿Cómo completar la partida?

7. ¿Cómo informar del resultado final de la partida?

Estas preguntas van a necesitar que se intercambie información entre los agentes de la plataforma. Para resolver las diferentes preguntas se presentan los diagramas UML que representan al protocolo elegido con para la secuencia de mensajes que deben intercambiarse entre los agentes implicados. El contenido de los mensajes representará a un elemento presente en la ontología.

### 1.1 ¿Cómo diferenciar a los diferentes agentes especializados?

Para resolver esta pregunta utilizaremos la utilidad del servicio de páginas amarillas que nos proporciona la plataforma de **agentes-FIPA**. De esta forma no será necesario tener elementos en la ontología para poder resolver el problema de comunicación pero sí será necesario un elemento en el vocabulario para que los agentes puedan subscribirse en el servicio de páginas amarillas de forma homogénea:

- `TIPO_SERVICIO` : Los agentes especializados deben registrase con esta constante del vocabulario como tipo de servicio para poder ser localizados por el agente monitor. Además cada uno de esos agentes deberá añadir el nombre de servicio asociado.

- `NombreServicio` : Es un enumerado que contiene los nombres de servicios asociados a los agentes especializados para esta práctica que deberán ser localizados.

	- `JUGADOR` : Representa el nombre del servicio asociado para cualquier agente ratón de cualquier jugador presente en la plataforma de agentes.

	- `ORGANIZADOR` : Representa el nombre del servicio que proporcionan los agentes laberinto que se encargarán de la organización de las partidas que representan un juego del que tendrán que completar su resultado.

El agente especializado que será el encargado en localizar a los otros dos tipos de agentes especializados será el `AgenteMonitor`. Además este agente recibirá como parámetro un fichero de configuración con la siguiente información:

- Lista de juegos, con todos los elementos necesarios, para los que tendrá que localizar los agentes necesarios para obtener un resultado.
- Lista de agentes jugadores y organizadores que tiene la responsabilidad de inicializar. Esto no significa que solo deberá comunicarse con ellos.

### 1.2 ¿Cómo proponer a los diferentes jugadores que participen en un juego?

El `AgenteMonitor` propondrá un juego, de la lista que se le ha suministrado a su inicialización, a los `AgenteRaton`. El juego debe identificarse de forma unívoca y los jugadores pueden tomar su decisión para participar en el juego atendiendo a su estado actual.

![][imagen1]

En el diagrama se presentan los elementos de la ontología que deberán formar parte del contenido del mensaje que se envía al agente. Los elementos de la ontología tendrán los siguientes atributos:

- `ProponerJuego` : Tiene la información necesaria para que los agentes puedan tomar su decisión
	- `Juego` : representa al juego en el que debe participar el jugador.
		- `idJuego` : identificará de forma unívoca el juego.
		- `ModoJuego` : será el modo en el que se desarrollan las partidas que forman el juego:
			- `BUSQUEDA` : solo un ratón por partida y el objetivo es conseguir los quesos.
			- `TORNEO` : todos los ratones se enfrentan para conseguir el mayor número de quesos en las partidas que componen el juego.
			- `ELIMINATORIA` : no todos los ratones compiten en todas las partidas. La idea es tener una última partida donde se enfrenten los mejores ratones para decidir el ganador del juego.
	- `DificultadJuego` : una medida para que el organizador pueda ajustar las partidas que componen el juego. Esta información también puede ser útil para los jugadores en sus estrategias:
		- `BUSQUEDA` : para el caso especial donde solo participa un ratón.
		- `FACIL` : para un número de ratones entre 2 y 4.
		- `NORMAL` : para un número de ratones entre 4 y 8.
		- `DIFICIL` : para un número de ratones entre 8 y 12.

- `JuegoAceptado` : Permite al agente indicar que desea participar en el juego. Ya sea como jugador u organizador.
	- `Juego` : representa el juego en el que desea participar el agente.
	- `AgenteJuego` : representa al agente especializado que desea participar en el juego. Es un concepto abstracto que permite representar a los agentes especializados y así poder extender la ontología para atender la posibilidad que se añadan más agentes especializados.

- `Justificacion` : Si no se acepta el juego se debe justificar los motivos, para ello utilizamos un elemento del vocabulario donde se encuentran los posibles motivos:
	- `Juego` : representa el juego en el que no se desea participar.
	- `Motivo` : justificación para no participar en el juego. Los posibles motivos estarán recogidos en el vocabulario:
```
JUEGOS_ACTIVOS_SUPERADOS | PARTICIPACION_EN_JUEGOS_SUPERADA |
TIPO_JUEGO_NO_IMPLEMENTADO | DEMASIADOS_JUEGOS_SIN_COMPLETAR | SUPERADO_LIMITE_PARTIDAS
| SUBSCRIPCION_ACEPTADA | ERROR_SUBSCRIPCION | ERROR_CANCELACION | ONTOLOGIA_DESCONOCIDA
```

Es responsabilidad de cada agente tratar correctamente todos los mensajes implicados en la comunicación y resolver las tareas asociadas para que no aborte su ejecución. Como caso particular, si el `AgenteRaton` no puede decodificar el contenido del mensaje deberá lanzar la excepción `NotUnderstoodException`.

### 1.3 ¿Cómo debe completarse un juego ?

Cuando el `AgenteMonitor` ha localizado jugadores que están dispuestos a participar en un juego hay que localizar a un `AgenteLaberinto` que se encargue de generar las partidas necesarias para completar ese juego.

![][imagen2]

En el diagrama se presentan los elementos de la ontología que deberán formar parte del contenido del mensaje que se envía al agente. Los elementos de la ontología tendrán los siguientes atributos:

- `OrganizarJuego` : Tiene la información necesaria para que el organizador pueda generar las partidas que permitan completar el juego propuesto. 
	- `Juego` : juego que se debe completar.
	- `DificultadJuego` : es una medida que permite al organizador establecer las condiciones necesarias para generar las partidas para ese juego.
	- `ListaJugadores` : es una colección de elementos `Jugador` que participarán en las partidas que definen el juego. El número será congruente con `ModoJuego`.

Los elementos restantes de la ontología ya han sido descritos anteriormente. Las incidencias también se resolverán como en el punto *1.2*.

### 1.4 ¿Cómo obtener el resultado del juego propuesto?

Para que el `AgenteMonitor` pueda recibir la información del resultado de los juegos que proponga a diferentes `AgenteLaberinto` será necesario el siguiente intercambio de mensajes:

![][imagen3]

En el diagrama se presentan los elementos de la ontología que deberán formar parte del contenido del mensaje que se envía al agente. Los elementos de la ontología tendrán los siguientes atributos:

- `InformarResultado` : Representa la información para poder establecer la suscripción.
	- `AgenteJuego` : Es un concepto abstracto que permite representar a los agentes especializados y así poder extender la ontología para atender la posibilidad que se añadan más agentes especializados. En este momento será el `Monitor` que estará representado por su equivalente `AgenteMonitor` en la plataforma.

- `SubInform` : Elemento abstracto para representar las posibilidades de información que puede recibir el agente especializado. En este caso los valores pueden ser:
	- `ClasificacionJuego` : Si el juego ha finalizado correctamente se envía la información relativa a la clasificación del juego con los jugadores implicados.
		- `Juego` : representa el juego que ha finalizado.
		- `ListaJugadores` : colección de elementos `Jugador` que han participado en el juego ordenados desde el campeón en adelante.
		- `ListaPuntuacion` : colección con los puntos obtenidos por cada uno de los jugadores de la lista anterior.
	- `IncidenciaJuego` : si la partida no finaliza de forma normal este elemento indicará el motivo
		- `Juego` : representa el juego que ha finalizado.
		- `Incidencia` : elemento del vocabulario que representa el motivo por el que el juego no ha terminado correctamente.

Los elementos restantes de la ontología ya han sido descritos anteriormente. Las incidencias también se resolverán como en el punto *1.2*.

### 1.5 ¿Cómo generar las partidas que componen un juego?

El `AgenteLaberinto` se encarga de ir generando las partidas necesarias para completar completar el juego. Utilizando `ModoJuego` y `DificultadJuego` el `AgenteLaberinto` debe establecer el método necesario para ello. El alumno es libre para establecer las partidas que estime oportuno, pero deberá estar debidamente justificado. Para cada partida se crea un `AgentePartida` al que se le pasarán los parámetros necesarios para que pueda generar el laberinto asociado a esa partida y pueda completarlo con la lista de jugadores asociada a esa partida. 

Cuando se complete la partida el AgentePartida debe suministrar la información necesaria para que se pueda establecer el resultado del juego una vez se completen todas sus partidas asociadas.

Hay que tener en cuenta en el diseño que se puede pedir la reproducción de una partida ya jugada.

### 1.6 ¿Cómo iniciar la partida? ¿Cómo completar un turno de una partida? ¿Cómo completar la partida?

El `AgentePartida` es el encargado de organizar los turnos que componen una partida. De esta forma lo importante es identificar los elementos de información necesarios para completar un turno de juego. 

#### Iniciar la partida
Los jugadores empiezan en la *posición (0,0)* del laberinto y necesitan conocer el entorno de esa posición antes de comenzar los turnos de juego. La *posición (0,0)* estará localizada en la esquina inferior izquierda del laberinto.  Para hacerlo es necesario el siguiente intercambio de mensajes entre los agentes implicados:

![][imagen4]

En el diagrama se presentan los elementos de la ontología que deberán formar parte del contenido del mensaje que se envía al agente. Los elementos de la ontología tendrán los siguientes atributos:

- `InicarPartida` : Contiene la información necesaria para que el jugador se de por enterado que ha empezado a jugar una nueva partida:
	- `Partida` : partida que da comienzo.
		- `idJuego` : identificador del juego al que corresponde la partida.
		- `idPartida` : identificador de la partida.
	- `Entorno` : localización de muros o espacio abierto de una posición, en este caso será la *posición (0,0)* que es donde se inicia la partida:
		- `ListaParedes` : es una lista de elementos `Pared` que serán localizadas mediante `Orientacion`. En el vocabulario están sus definiciones.
	- `bombas` : número de bombas disponibles para la partida.

- `EstadoPartida` : Información sobre posibles contingencias que pueden ocurrir durante la partida.
	- `Partida` : partida que da comienzo.
	- `Estado` : posibles estados en los que se puede encontrar la partida. Sus valores están definidos en el vocabulario.
		- [`GANADOR` | `ABANDONO` | `SEGUIR_JUGANDO` | `FIN_PARTIDA` | `JUGADOR_NO_ACTIVO`]

#### Turno de la partida

Para completar la partida hay que ir generando los turnos necesarios hasta obtener un ganador o declarar un empate.

![][imagen5]

En el diagrama se presentan los elementos de la ontología que deberán formar parte del contenido del mensaje que se envía al agente. Los elementos de la ontología tendrán los siguientes atributos:

- `PedirMovimiento` : indicará a los jugadores que deberán realizar un nuevo movimiento en la partida
	- `Partida` : partida que se está jugando.
	- `Posicion` : indica la posición en la que se encuentra el queso en este momento. 
		- `corX` : coordenada **X** de la posición en el laberinto. Es la coordenada **X** de un sistema cartesiano, inferior del laberinto.
		- `corY` : coordenada **Y** de la posición en el laberinto. Es la coordenada **Y** en un sistema cartesiano, lateral del laberinto.

- `MovimientoEntregado` : acción del turno para el jugador
	- `Partida` : partida que se está jugando.
	- `Movimiento` : movimiento realizado por el jugador
		- `Accion` : acción que realiza, es un elemento definido en el vocabulario.
		- `Posicion` : posición asociada a la acción. Si es un movimiento será la nueva casilla donde se desplaza el ratón. Si es una bomba será la posición adyacente donde se coloca la bomba. El ratón no cambia su posición en el laberinto.

- `EntornoLaberinto` : como resultado del movimiento se comunica a los jugadores el estado del laberinto asociado a su nueva posición según la acción que han llevado a cabo en el turno.
	- `Partida` : partida que se está jugando.
	- `Posicion` : posición asociada a su movimiento. 
	- `Entorno` : entorno de la casilla del laberinto asociada a la posicion.

Los elementos restantes de la ontología ya han sido descritos anteriormente. Las incidencias también se resolverán como en el punto *1.2*.

### 1.7 ¿Cómo informar del resultado final de la partida?

Como el `AgentePartida` es creado por un `AgenteLaberinto` esta comunicación es propia y estará asociada al diseño que cada grupo desee hacer para sus agentes. La información que deberá intercambiarse también depende de cada grupo pero debe ser la necesaria para poder computar el resultado final del juego que deberá comunicarse al `AgenteMonitor` que solicitó su organización.

Hay que tener en cuenta en el diseño que deberá suministrarse la información necesaria para que el `AgenteLaberinto` pueda solicitar la reproducción de la partida jugada.

## 2 Diseño de la ontología

En la sección anterior se han presentado todos los elementos y relaciones que se presentan en la ontología necesaria para completar los juegos propuesto. Para el diseño de la ontología se tendrá presente su implementación con las capacidades que nos proporciona la biblioteca de agentes [Jade](https://jade.tilab.com/doc/api/jade/content/package-frame.html). De esta forma se presentarán unos diagramas de clase donde se muestran las relaciones entre ellas. Se presentan tres diagramas atendiendo a los elementos:

- `Concept` : Elementos que representan la información necesaria para representar los diferentes tipos de juegos de la ontología.

- `AgentAction` : Elementos que representas los eventos a los que responden los agentes para completar los juegos de la ontología. Cuando se genere el contenido del mensaje utilizando uno de estos elemento de la ontología hay que incluirlo en un objeto [`Action`](https://jade.tilab.com/doc/api/jade/content/onto/basic/Action.html) presente en el API de la biblioteca. Este será el objeto con el que se genera el contenido del mensaje y el objeto que se recupera cuando se recupera el contenido del mensaje.

- `Predicate` : Elementos que representan las respuestas a los eventos para completar los juegos de la ontología.

Para el diseño de la ontología se ha creado la siguiente estructura de paquetes:

- `es.ujaen.ssmmaa.ontomouserun` : paquete principal de la ontología
	- `OntoMouseRun` : clase que representa la ontología para la práctica.
	- `Vocabulario` : interface Java donde se encuentran los elementos de vocabulario utilizados en el diseño de los elementos de la ontología.

- `es.ujaen.ssmmaa.ontomouserun.elementos` : paquete donde se encuentran todos los elementos propios de la ontología.
	- `AgenteJuego` : destaco este elemento porque es un elemento abstracto que sirve para identificar cualquier elemento asociado a los elementos que representan a cualquier agente que se necesita identificar dentro del diseño de la práctica.
		- `Jugador` : identifica a un jugador del juego representado por su agente ratón.
		- `Organizador` : identifica al agente laberinto asociado que es el encargado de organizar las partidas mediante sus agentes partida.
		- `Monitor` : identifica al agente monitor que controla el desarrollo de los juegos.
	- `SubInform` : es otro elemento abstracto para poder identificar cualquier mensaje que se comunique en una suscripción relacionada con el diseño de la práctica.
		- `IncidenciaJuego` : para comunicar cualquier incidencia que se deba conocer en la organización de un juego.
		- `ClasificacionJuego` : representa la resolución efectiva de un juego con una lista ordenada según puntuación de los jugadores que han participado en un juego.

## 3 Uso de la ontología para el desarrollo de la práctica

Para utilizar la ontología necesitamos acceder a las definiciones que se encuentran en la biblioteca de la ontología. Como en las prácticas estamos utilizando el gestor de dependencias [**Maven**](https://es.wikipedia.org/wiki/Maven) para nuestros proyectos Java debemos hacer las siguientes modificaciones en el fichero `pom.xml`:

Dependencias asociadas a la biblioteca de la ontología.

```xml
<repositories>
	<repository>
		<id>tilab</id>
		<url>https://jade.tilab.com/maven/</url>
	</repository>
</repositories>

<dependencies>
	<dependency>
		<groupId>com.tilab.jade</groupId>
		<artifactId>jade</artifactId>
		<version>4.6.0</version>
	</dependency>
	<dependency>
		<groupId>es.ujaen.ssmmaa</groupId>
		<artifactId>OntoMouseRun</artifactId>
		<version>1.0</version>
	</dependency>
</dependencies>
```

Para que tener acceso a las dependencias de la ontología se instalará manualmente en el ordenador del alumno. Para ello debemos descargar el fichero [`OntoMouseRun-1.0.jar`](https://platea.ujaen.es/pluginfile.php/173260/mod_assign/introattachment/0/OntoMouseRun-1.0.jar?forcedownload=1) que se encuentra asociado a la actividad en Platea. Posteriormente hay que ejecutar la sentencia maven:

```
mvn install:install-file -Dfile=DIRECTORIO_DE_LA_DESCARGA/OntoMouseRun-1.0.jar -DgroupId=es.ujaen.ssmmaa -DartifactId=OntoMouseRun -Dversion=1.0 -Dpackaging=jar
```

Si hay actualizaciones en el proyecto de ontología se avisará a los alumnos para que hagan la actualización de la referencia de la ontología que se indicará en este documento siguiendo las instrucciones que se han presentado en los párrafos anteriores.

[imagen1]:https://gitlab.com/ssmmaa/curso2022-23/ontomouserun/-/raw/master/img/ProponerJuego.png

[imagen2]:https://gitlab.com/ssmmaa/curso2022-23/ontomouserun/-/raw/master/img/CompletarJuego.png

[imagen3]:https://gitlab.com/ssmmaa/curso2022-23/ontomouserun/-/raw/master/img/InformarJuego.png

[imagen4]:https://gitlab.com/ssmmaa/curso2022-23/ontomouserun/-/raw/master/img/IniciarPartida.png

[imagen5]:https://gitlab.com/ssmmaa/curso2022-23/ontomouserun/-/raw/master/img/PedirMovimiento.png

